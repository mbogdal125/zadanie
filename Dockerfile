FROM ubuntu:latest
MAINTAINER Marta Bogdal
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential
COPY . /zadanie
WORKDIR /zadanie
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["run.py"]