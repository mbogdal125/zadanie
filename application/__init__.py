import flask
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api
from sqlalchemy import *
from flask_migrate import Migrate

app = flask.Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)

migrate = Migrate(app, db)

api = Api(app)

from application.models import Client, Dataset
db.create_all()

from application.controllers import ClientListResource, ClientResource, DatasetResource, DatasetListResource

api.add_resource(ClientListResource, '/clients', endpoint='clients')
api.add_resource(ClientResource, '/clients/<string:id>', endpoint='client')

api.add_resource(DatasetListResource, '/datasets', endpoint='datasets')
api.add_resource(DatasetResource, '/datasets/<string:id>', endpoint='dataset')

# Here is example code how to build RESTful JSON APIs with Flask-Potion. It handles client request on urls: /client and /client/<id>
from flask_potion import Api, ModelResource, fields
apipotion = Api(app)
from application.controllers import ClientResourcePotion
apipotion.add_resource(ClientResourcePotion)
