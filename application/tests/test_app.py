import unittest
import json

from application import app, db

class AppTests(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
        self.app = app.test_client()
        db.drop_all()
        db.create_all()
        self.assertEqual(app.debug, False)

    def tearDown(self):
        db.drop_all()

    def test_get_clients_if_empty(self):
        response = self.app.get('/clients', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        clients = json.loads(response.data)
        self.assertEqual(len(clients), 0)

    def test_client_with_wrong_ip_format(self):
        response = self.app.post('/clients', data={'name':'Ann', 'ip_address':'131.'}, follow_redirects=True)
        self.assertEqual(response.status_code, 409)

    def test_client_add_and_get_all(self):
        response = self.app.post('/clients', data={'name':'Ann', 'ip_address':'131.2.3.4'}, follow_redirects=True)
        self.assertEqual(response.status_code, 201)
        response = self.app.get('/clients', follow_redirects=True)
        datasets = json.loads(response.data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(datasets), 1)

    def test_get_dataset_if_empty(self):
        response = self.app.get('/datasets', follow_redirects=True)
        datasets = json.loads(response.data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(datasets), 0)

    def test_add_alient_assign_to_dataset_and_get_all(self):
        response = self.app.post('/clients', data={'name':'Ann', 'ip_address':'131.2.3.4'}, follow_redirects=True)
        self.assertEqual(response.status_code, 201)
        response = self.app.get('/clients/1', follow_redirects=True)
        client1 = json.loads(response.data)
        self.assertEqual(response.status_code, 200)

        response = self.app.post('/datasets', data={'filename':'dataset1.json', 'client':client1['id']}, follow_redirects=True)
        response = self.app.get('/datasets', follow_redirects=True)
        datasets = json.loads(response.data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(datasets), 1)

if __name__ == "__main__":
    unittest.main()
