import re
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy_utils import JSONType
from sqlalchemy.orm import validates
from flask_restful import reqparse, Resource, Api, abort, fields, marshal_with

from application import db

class Client(db.Model):
    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128),  nullable=False)
    ip_address = db.Column(db.String(15),  nullable=False)

    @validates('ip_address')
    def validate_ip_address(self, key, ip_address):
        # here is only string validation, you can also use ipaddress library to check if ip is valid.
        pat = re.compile("\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}")
        test = pat.match(ip_address)
        if not test:
            abort(409, message="Unknown ip address format.")
        return ip_address

    def __repr__(self):
        return '<Client %r>' % self.filename

class Dataset(db.Model):
    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    id = db.Column(db.Integer, primary_key=True)
    client = db.Column(db.Integer, db.ForeignKey('client.id'),
                       nullable=False)
    filename = db.Column(db.String(128),  nullable=False)
    metadata_ds = db.Column(JSONType,  nullable=True)
    def __repr__(self):
        return '<Dataset %r>' % self.filename
