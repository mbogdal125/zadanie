from flask_restful import reqparse, Resource, Api, abort, fields, marshal_with, marshal_with_field

from application import db
from application.models import Dataset,Client

client_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'ip_address': fields.String,
}

dataset_fields = {
    'id': fields.Integer,
    'filename': fields.String,
    'client': fields.String,
    'metadata_ds': fields.Raw, # here is String field type. In futher development it should be implemented fields.Nested field to return parsed json
}

parser = reqparse.RequestParser()
parser.add_argument('name', type=str)
parser.add_argument('ip_address', type=str)

parser_dataset = reqparse.RequestParser()
parser_dataset.add_argument('filename', type=str)
parser_dataset.add_argument('client', type=str)
parser_dataset.add_argument('metadata_ds', type=str)

class ClientResource(Resource):
    @marshal_with(client_fields)
    def get(self, id):
        client = db.session.query(Client).filter_by(id=id).first()
        if not client:
            abort(404, message="CLient {} doesn't exist".format(id))
        return client

    def delete(self, id):
        client = db.session.query(Client).filter(id == id).first()
        if not client:
            abort(404, message="Client {} doesn't exist".format(id))
        db.session.delete(client)
        db.session.commit()
        return {}, 204

    @marshal_with(client_fields )
    def put(self, id):
        parsed_args = parser.parse_args()
        client = db.session.query(Client).filter(id == id).first()
        if parsed_args['name']:
            client.name = parsed_args['name']
        if parsed_args['ip_address']:
            client.ip_address = parsed_args['ip_address']
        db.session.add(client)
        db.session.commit()
        return client, 201


class ClientListResource(Resource):
    @marshal_with(client_fields)
    def get(self):
        clients = db.session.query(Client).all()
        return clients

    @marshal_with(client_fields)
    def post(self):
        parsed_args = parser.parse_args()
        client = Client(name=parsed_args['name'], ip_address=parsed_args['ip_address'])
        db.session.add(client)
        db.session.commit()
        return client, 201


class DatasetResource(Resource):
    @marshal_with(dataset_fields)
    def get(self, id):
        dataset = db.session.query(Dataset).filter_by(id=id).first()
        if not dataset:
            abort(404, message="Dataset {} doesn't exist".format(id))
        return dataset

    def delete(self, id):
        dataset = db.session.query(Dataset).filter(id == id).first()
        if not dataset:
            abort(404, message="Dataset {} doesn't exist".format(id))
        db.session.delete(dataset)
        db.session.commit()
        return {}, 204

    @marshal_with(dataset_fields)
    def put(self, id):
        parsed_args_dataset = parser_dataset.parse_args()
        print(parsed_args_dataset)
        dataset = db.session.query(Dataset).filter(id == id).first()
        if parsed_args_dataset['filename']:
            dataset.filename = parsed_args_dataset['filename']
        if parsed_args_dataset['filename']:
            dataset.filename = parsed_args_dataset['filename']
        if parsed_args_dataset['metadata_ds']:
            dataset.ip_address = parsed_args_dataset['metadata_ds']
        db.session.add(dataset)
        db.session.commit()
        return dataset, 201


class DatasetListResource(Resource):
    @marshal_with(dataset_fields)
    def get(self):
        dataset = db.session.query(Dataset).all()
        return dataset

    @marshal_with(dataset_fields)
    def post(self):
        parsed_args_dataset  = parser_dataset.parse_args()
        dataset = Dataset(filename=parsed_args_dataset['filename'], client=parsed_args_dataset['client'],metadata_ds=parsed_args_dataset['metadata_ds'])
        db.session.add(dataset)
        db.session.commit()
        return dataset, 201


# Here is example code how to build RESTful JSON APIs with Flask-Potion.

from flask_potion import Api, ModelResource, fields
from application.models import Client, Dataset

class ClientResourcePotion(ModelResource):
    class Meta:
        model = Client
