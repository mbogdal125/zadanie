# Zadanie

Implementacja REST API w wybranym frameworku (Pyramid / Flask) z wykorzystaniem SQLAlchemy
(dowolna baza SQL).

## Opis

REST API zostalo zaimplementowane we frameworku Flask z użyciem dodatkowych bibliotek t.j:Flask-RESTful, Flask-SQLAlchemy.
W zadaniu zostało także zaprezentowane użycie biblioteki Flask-Potion

Do zadania została użyta baza SQLite.


### Instalacja

Aby zainstalować projekt należy stworzyć środowisko wirtualne (virtualenv) oraz zainstalować wymagania z pliku requirements.txt

pip install -r requirements.txt

### Implementacja REST API

REST API udostępnia następujące endpoint'y:

- /clients [POST, GET]
dane: name<string>, ip_address<string> format <s>.<s>.<s>.<s>

- /clients/1 [GET, PUT]

- /datasets [POST, GET]
dane: client<integer>, filename<string>, metadata_df<dict>

- /datasets/1 [GET, PUT]

```
Przyklad
```

POST /clients HTTP/1.1
Content-Type: application/x-www-form-urlencoded; charset=utf-8
Host: localhost:5000
Connection: close
User-Agent: Paw/3.1.4 (Macintosh; OS X/10.11.6) GCDHTTPRequest
Content-Length: 32

name='Kate'&ip_address=131.1.1.1

W konsoli można API przetestować np. z wykorzystaniem biblioteki requests:
pip install requests
requests.get("http://localhost:5000/clients")

### Implementacja REST API z biblioteką Flask-Potion

- /client [POST, GET] dane: name<string>, ip_address<string> format <s>.<s>.<s>.<s>
- /client/1 [GET, PUT]

### Testy
Zostały zaimplementowane testy jednostkowe z użyciem unittest w pliku application/tests/test_app.py

### Migracja
Zostały zaimplementowana migracja bazy danych z użyciem Flask-Migrate

Inicjalizacja bazy danych: flask db init
Migracja zmian: flask db migrate
Wprowadzenie migracji do bazy: flask db upgrade

### Docker

Aby zbudować obraz należy zainstalować docker, a następnie uruchomić komendę:
docker build -t zadanie:latest .

Plik konfiguracyjny znajduje się w głównym katalogu: Dockerfile